import asyncio
import os

# 1- Write a python function that finds the duplicate items in any given array.
def find_duplicate_items(items: list) -> set:
    duplicates = []
    array_checked = []

    for item in items:
        if item in array_checked:
            duplicates.append(item)
        array_checked.append(item)

    return set(duplicates)

# print(find_duplicate_items([1, 1, 2, 3, 2, 1, 3, 4, 6]))

# 2 - Write an async python function that writes every item in any given array with 1, 2, 4, 8, ... seconds apart.
#  ex: for [“a”,” b, “c, “d”], “a” is printed in 1 sec, “b” is printed in 2 seconds,
#  “c” is printed in 4 seconds, ...                                  
async def delay_print_elements(elements: list) -> None:
    delay = []

    for index, item in enumerate(elements):
        if len(delay) < 1:
            delay.append(1)
        else:
            aux = delay[-1] * 2
            delay.append(aux)

        print(item)
        await asyncio.sleep(delay[-1])

elements = ["a", "b", "c", "d"]
# asyncio.run(
#     delay_print_elements(elements)
# )

# 4 -Write an efficient method that tells us whether or not an input string brackets ("{", "}",                                 
# "(", ")", "[", "]") opened and closed properly. Example: “{[]}” => true, “{(])}” => false,                             
# “{([)]}” => false
def brackets_closed_properly(brackets_string) -> bool:
    stack = []
    brackets = "()[]{}"

    for bracket in brackets_string:
        if bracket not in brackets:
            continue

        bracket_index = brackets.index(bracket)

        if bracket_index % 2 == 0:
            stack.append(bracket)
        else:
            if brackets[bracket_index - 1] == stack[-1]:
                stack.pop()
            else:
                return False
    
    return not len(stack)

# print(brackets_closed_properly("{([)]}"))

# 6 - Write the code that animates “Zeno's Paradox of Achilles and the Tortoise” on an interface(we would like to see the paradox demonstrated).
async def paradox_achilles_and_tortoise() -> None:
    achilles_speed = 30
    tortoise_speed = 15
    achilles_position = 0
    tortoise_position = 50
    length_road = 100

    while tortoise_position < length_road or achilles_position < length_road:
        os.system("clear")
        print(f"Achilles: {achilles_position}%\nTortoise: {tortoise_position}%")

        for x in range(length_road):
            if tortoise_position / 100 * length_road == x:
                print("*", end="")
            elif achilles_position / 100 * length_road == x:
                print("|", end="")
            else:
                print("-", end="")
        
        print("\n")

        tortoise_position += tortoise_speed
        achilles_position += achilles_speed

        await asyncio.sleep(1)

# asyncio.run(paradox_achilles_and_tortoise())

# 5- A building has 100 floors. One of the floors is the highest floor an egg can be dropped
# from without breaking. If an egg is dropped from above that floor, it will break. If it is
# dropped from that floor or below, it will be completely undamaged and you can drop the
# egg again. Given two eggs, find the highest floor an egg can be dropped from without
# breaking, with as few drops as possible on the worst-case scenario.
#n(n+1) / 2 >= 100

def drop_eggs(building):
    pass

# print(drop_eggs(100))

# 7 - Think that you have an unlimited number of carrots, but limited number of carrot
# types. Also, you have one bag that can hold a limited weight. Each type of carrot has a
# weight and a price. Write a function that takes carrotTypes and capacity and return the
# maximum value the bag can hold.
def get_max_value(carrot_types: list, capacity: float) -> float:
    result = []

    for carrot_type in carrot_types:
        price_by_kg = carrot_type['price'] / carrot_type['kg']
        quantity_carrot = capacity / carrot_type['kg']
        value_by_bag = quantity_carrot * carrot_type['price']

        result.append({
            'quantity': int(quantity_carrot),
            'value': int(value_by_bag)
        })

    carrot = {}
    carrot['quantity'] = sorted(result, key=lambda carrot: carrot['quantity']).pop()['quantity']
    carrot['value'] = sorted(result, key=lambda carrot: carrot['value']).pop()['value']

    return carrot

carrot_types = [
    { 
        "kg": 5,
        "price": 100,
    },
    { 
        "kg": 7,
        "price": 180,
    },
    {
        "kg": 3,
        "price": 70
    }
]

bag_capacity = 36

print(get_max_value(carrot_types, bag_capacity))
